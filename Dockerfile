FROM centos:8

# В CentOS8 была бага с настройками реп, потому её надо пофиксить
RUN cd /etc/yum.repos.d/ \
    && sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-* \
    && sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-* \
    && dnf install -y python39 \
    && pip3 install flask flask_jsonpify flask_restful \
    && mkdir -p /python_api \
    && dnf clean all

COPY ./python-api.py /python_api

RUN chmod -R 755 /python_api

WORKDIR /python_api

ENTRYPOINT /usr/bin/python3 ./python-api.py
